#include <gtest/gtest.h>

void valeurs(int a){
    a = 30;
}
void references(int &a){
    a = 40;
}
void pointers(int *a){
    *a = 20;
}

class A{
private:
    int m_m = 0;
public:
    A(){}
    int getM() const {return m_m;}
    void setM(const int m){m_m = m;}
};

void aSetMValeurs(A a, int m){a.setM(m);}
void aSetMReferences(A &a, int m){a.setM(m);}
void aSetMPointers(A *a, int m){a->setM(m);}

TEST(Pointers, init){
    int a(10);
    int *p(&a);
    ASSERT_EQ(p, &a);
    ASSERT_EQ(*p, a);
}

TEST(Pointers, functions_simple){
    int x(10);
    int *p(&x);

    valeurs(x);
    ASSERT_EQ(x, 10);
    references(x);
    ASSERT_EQ(x, 40);
    pointers(&x);
    ASSERT_EQ(x, 20);
    ASSERT_EQ(*p, 20);
    references(*p);
    ASSERT_EQ(*p, 40);
    valeurs(*p);
    ASSERT_EQ(*p, 40);
    pointers(p);
    ASSERT_EQ(*p, 20);
}

TEST(Pointers, functions_class){
    A a;
    A *p = new A();
    ASSERT_EQ(p->getM(), 0);
    ASSERT_EQ(a.getM(), 0);
    aSetMValeurs(a, 10);
    aSetMValeurs(*p, 10);
    ASSERT_EQ(p->getM(), 0);
    ASSERT_EQ(a.getM(), 0);
    aSetMReferences(a, 10);
    aSetMReferences(*p, 10);
    ASSERT_EQ(p->getM(), 10);
    ASSERT_EQ(a.getM(), 10);
    aSetMPointers(&a, 30);
    aSetMPointers(p, 30);
    ASSERT_EQ(p->getM(), 30);
    ASSERT_EQ(a.getM(), 30);
}
