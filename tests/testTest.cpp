#include "test.h"
#include <gtest/gtest.h>

TEST(StringFromInt, positive){
    ASSERT_EQ("1", generate_string_from_nb(1));
    ASSERT_EQ("1000", generate_string_from_nb(1000));
    ASSERT_EQ("0", generate_string_from_nb(0));
}

TEST(StringFromInt, negative){
    ASSERT_EQ("-1", generate_string_from_nb(-1));
    ASSERT_EQ("-1000", generate_string_from_nb(-1000));
    ASSERT_EQ("0", generate_string_from_nb(-0));
}

TEST(StringFromInt, other){
    ASSERT_EQ("-1", generate_string_from_nb(-1));
    ASSERT_EQ("-1000", generate_string_from_nb(-1000));
    ASSERT_EQ("0", generate_string_from_nb(-0));
}
