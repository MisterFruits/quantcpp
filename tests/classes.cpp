#include <gtest/gtest.h>
#include <typeinfo>

class A{
public:
    int return10(){return 10;}
private:
    int retunr60(){return 60;}
friend class C;
};

class C{
public:
    int aReturn60(A a){return a.retunr60();}
};

class B : public A{
    
};

TEST(Classes, inheritance){
    A *a;
    B *b = new B();
    a = b;
    ASSERT_EQ(a->return10(), 10);
    ASSERT_EQ(b->return10(), 10);
    B c = B();
    ASSERT_EQ(c.return10(), 10);
    a = &c;
}

TEST(Classes, friends){
    A a;
    B b;
    C c;
    ASSERT_EQ(c.aReturn60(a), 60);
    ASSERT_EQ(c.aReturn60(b
    
    ), 60);
}

TEST(Classes, casts){
    float x = 1.;
    int a = 1;
    ASSERT_FALSE(typeid(x) == typeid(a));
    ASSERT_EQ(typeid(x), typeid(x));
    ASSERT_EQ(typeid(x), typeid(x + a));
    ASSERT_EQ(typeid(a), typeid(static_cast<int>(a + x)));
    // static_cast<int>(a + x);
}