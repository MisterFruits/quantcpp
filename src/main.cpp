#include <iostream>
#include <string>
#include <cmath>  //Ne pas oublier cette ligne 

using namespace std;


int main2()
{
    std::string nomUtilisateur("Albert Einstein");
    std::sqrt(static_cast<double>(3.141592654));
    std::sin(3.45);
    return 0;
}


  
class A
{
  protected:
    int x;
  public:
    A() { x = 0;}
  friend void show();
};
  
class B: public A
{
  public:
    B() : y (0) {}
  private:
    int y;
  
};
  
void show()
{
  B b;
  cout << "The default value of A::x = " << b.x;
  
  // Can't access private member declared in class 'B'
//   cout << "The default value of B::y = " << b.y;
}
  
int main()
{
  show();
  getchar();
  return 0;
}